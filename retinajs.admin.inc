<?php

/**
 * @file
 * Install, update and uninstall functions for the retinajs module.
 */

/**
 * Administration form.
 */
function retinajs_admin_form() {
  $form = array();
  
  // Global upscale setting
  $form['retinajs_upscale'] = array(
    '#title' => t("Force upscale"),
    '#description' => t("Override image style settings when serving a retina image"),
    '#type' => 'checkbox',
    '#default_value' => variable_get('retinajs_upscale', 0),
  );
  
  return system_settings_form($form);
}
